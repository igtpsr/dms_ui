export const environment = {
  production: true,
  baseUrl: '/BRIM-Combined/authApi/',
  migrationUrl: '/BRIM-Combined/migration/',
  imageUrl: '/brimtest/assets/retelzy-logo.png',
  rimDocs:'/brimtest/assets/RIM.pdf'
};


