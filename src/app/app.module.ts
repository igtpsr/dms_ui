import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { SidebarComponent } from "./_layout/sidebar/sidebar.component";
import { ConfigurationComponent, AddNewEnvironment, ConfirmMigrateDialogConfig } from "./configuration/configuration.component";
import { MainComponent, ConfirmMigrateDialog,ConfirmRestoreDialog, ProgressDialog, RestoreProgressDialog, DatePickerDialog } from "./main/main.component";
import { DashboardComponent, DashboardFilter } from "./dashboard/dashboard.component";
import { ErrorComponent } from "./error/error.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HeaderComponent } from "./_layout/header/header.component";
import { SiteLayoutComponent } from "./_layout/site-layout/site-layout.component";
import { TestComponent } from "./test/test.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthGuard } from "./guards/auth-guard.service";
// import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgProgressModule } from "ngx-progressbar";
import { NgProgressHttpModule } from "ngx-progressbar/http";
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatRadioModule } from '@angular/material/radio';
import { UserSettingsComponent, AddUser } from './_user/user-settings/user-settings.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { MatSelectModule } from '@angular/material/select';
import { TokenInterceptorService } from './token-interceptor.service';
import { NgIdleKeepaliveModule } from "@ng-idle/keepalive";
import { RoleProfileComponent,AddRole } from './_user/role-profile/role-profile.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    SidebarComponent,
    ConfigurationComponent,
    MainComponent,
    DashboardComponent,
    ErrorComponent,
    LoginComponent,
    HeaderComponent,
    SiteLayoutComponent,
    TestComponent,
    UserSettingsComponent,
    AddUser,
    ConfirmMigrateDialog,
    ConfirmRestoreDialog,
    ProgressDialog,
    RestoreProgressDialog,
    AddNewEnvironment,
    DashboardFilter,
    ConfirmMigrateDialogConfig,
    DatePickerDialog,
    RoleProfileComponent,
    AddRole
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgIdleKeepaliveModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgProgressModule.withConfig({
      spinnerPosition: "right",
      color: "#f71cff",
    }),
    NgProgressHttpModule,
    ToastrModule.forRoot({
      timeOut: 2000,
    }),
    ToastContainerModule,
    MatRippleModule,
    MatIconModule,
    MatProgressBarModule,
    MatButtonModule,
    MatTooltipModule,
    MatMenuModule,
    MatSidenavModule,
    MatRadioModule,
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule,
    MatSnackBarModule,
    DragDropModule,
    MatTabsModule,
    MatFormFieldModule,
    NgxSkeletonLoaderModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    Ng2GoogleChartsModule,
    MatSelectModule,
  ],
  providers: [AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
