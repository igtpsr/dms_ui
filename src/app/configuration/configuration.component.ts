import { Component, OnInit, Output, Input, Inject, ViewChildren, QueryList, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { Animations } from '../animation';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder, NgForm } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import { ActivatedRoute } from '@angular/router';
import { Environment } from './environment';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css'],
  animations: [
    Animations.animeTrigger
  ]

})
export class ConfigurationComponent implements OnInit, AfterViewInit {

  environmentGroup: FormGroup;
  moduleGroup: FormGroup;
  envType: string;
  @ViewChild('tabGroup') tabGroup;
  showPage: boolean = false;
  envDatalists: any = [];
  hide:boolean = false;
  existingSortedEnvList = ['DEV','TEST','QA','PROD'];
  environmentList = [
    {
      name:'Dev',
      disabled:'false'
    },
    {
      name:'Test',
      disabled:false
    },
    {
      name:'Prod',
      disabled:false
    },
    {
      name:'Qa',
      disabled:false
    }
    ];

  environmentType: any = new Environment(null, '', '', '', null, '', '', '', 0);

  constructor(private elRef: ElementRef, private formBuilder: FormBuilder, private adminService: AdminService, private Activrouter?: ActivatedRoute, private toastr?: ToastrService, public dialog?: MatDialog) {
    this.Activrouter.url.subscribe(url => {
      this.adminService.changeMessage(url[0].path);
    });

  }
  ngAfterViewInit() {
    // console.log(this.tabGroup.selectedIndex);
  }


  ngOnInit() {
    this.getEnvironmentList();
  }

  selectedTab(tab: any) {

    this.adminService.startLoader();
    this.envType = tab.tab.textLabel.toLowerCase();
    this.environmentType = this.envDatalists[tab.index];
    this.adminService.endLoader();

  }

  getEnvironmentList() {
    this.adminService.startLoader();
    let tempList = [];
    let unSortedEnvList = [];
    this.adminService.getEnvironment().subscribe(res => {

      unSortedEnvList = res;
      this.environmentList = this.environmentList.filter(res=>{
       if(unSortedEnvList.findIndex(i => i.dbEnvironmentType == res.name.toUpperCase())>-1) {
        return res.disabled = true;
       }
       return res;
      })


      tempList = new Array(unSortedEnvList.length);
      unSortedEnvList.forEach(data=>{
        var index = this.existingSortedEnvList.indexOf(data.dbEnvironmentType);
        tempList[index] = data;
      });

      tempList.forEach(data=>{
        if(data){
          this.envDatalists.push(data);
        }
      });
  
      unSortedEnvList.forEach(data=>{
        var index = this.existingSortedEnvList.indexOf(data.dbEnvironmentType);
        if(index === -1){
          this.envDatalists.push(data);
        }
      });
      //this.environmentType = this.envDatalists[0];
      this.adminService.endLoader();
      //setTimeout(() => {this.showPage = true; }, 600);
    })
  }

  deleteEnv(id: number) {

    const dialogRef = this.dialog.open(ConfirmMigrateDialogConfig, {
      width: '380px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.adminService.startLoader();
        this.adminService.removeTheDBEnvironmentDetails(id).subscribe(res => {
          if (res.Status.toUpperCase() == 'SUCCESS') {
            this.toastr.success('Successfully Deleted');
            setTimeout(()=>{
                location.reload();
            },2000)
          } else {
            this.toastr.success('Something Went Wrong');
          }
          this.adminService.endLoader();
          //this.envDatalists.splice(this.tabGroup.selectedIndex-1,1);
          //this.tabGroup.selectedIndex
        })

      }
    });
  }

  onNewFormSubmit() {
    this.adminService.startLoader();
    this.adminService.saveEnvironment(this.environmentType).subscribe(res => {
      this.adminService.endLoader();
      this.toastr.success('Successfully Submitted');
      this.environmentType.clientDbCredentialId = res.clientDbCredentialId;

    })
  }

  onUpdateFormSubmit() {
    this.adminService.startLoader();
    this.environmentType.isThisDbEnvVerified = 0;
    this.adminService.updateEnvironment(this.environmentType).subscribe(res => {
      this.adminService.endLoader();
      this.toastr.success('Successfully Updated');
    })
  }

  addNewEnv(): void {

    const dialogRef = this.dialog.open(AddNewEnvironment, {
      width: '380px',
      data:{environmentList:this.environmentList}
    });
    dialogRef.afterClosed().subscribe(result => {
      let data: string = result;
      if (data.length > 0) {
        let newEnvObj = new Environment(null, '', '', '', null, '', '', data, 0);
        this.envDatalists.push(newEnvObj);
        this.tabGroup.selectedIndex = this.envDatalists.length - 1;
      }
    });
  }

  checkConnection(): void {
    this.adminService.startLoader();
    this.adminService.testTheDBConnection(this.environmentType).subscribe(res=>{
      if(res.Status.toUpperCase() == 'SUCCESS') {
          this.toastr.success('Connection Verified');
          this.environmentType.isThisDbEnvVerified = 1;
      } else {
        this.toastr.error('Problem with connection, Please contact Admin');
      }
      this.adminService.endLoader();
    })
  }

}

@Component({
  selector: 'add-new-environment',
  templateUrl: 'add-new-environment.html'
})
export class AddNewEnvironment {
  // constructor(public dialogRef: MatDialogRef<AddNewEnvironment>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  constructor(public dialogRef: MatDialogRef<AddNewEnvironment>, @Inject(MAT_DIALOG_DATA) public data) { }
  environmentName: string;
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'confirm-migrate-dialog',
  templateUrl: 'confirm-migrate-dialog.html'
})
export class ConfirmMigrateDialogConfig {
  constructor(public dialogRef: MatDialogRef<ConfirmMigrateDialogConfig>) { }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
