export class Environment {
    dbEnvironmentType:string;
    isThisDbEnvVerified:number;
    clientDbCredentialId:number;
    constructor(clientDbCredentialId:number, dbUsername:string, dbPassword:string, dbServerName:string, portNumber:number, dbName:string, dbType:string, dbEnvironmentType:string, isThisDbEnvVerified:number) {
        this.clientDbCredentialId = clientDbCredentialId;
        this.dbEnvironmentType = dbEnvironmentType;
        this.isThisDbEnvVerified = isThisDbEnvVerified;
    }
}