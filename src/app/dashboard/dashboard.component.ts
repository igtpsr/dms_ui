import { Component, OnInit, Output, Input, Inject, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Animations } from '../animation';
// import { FormGroup, Validators, FormControl, FormArray, ValidatorFn } from '@angular/forms';
import * as _ from "lodash";
import { AdminService } from '../services/admin.service';
// import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
// import { ThemePalette } from '@angular/material/core';
// import { ProgressBarMode } from '@angular/material/progress-bar';
// import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Label } from './label'
// import { GoogleChartInterface } from 'ng2-google-charts';
// import { ChartReadyEvent, ChartErrorEvent, ChartSelectEvent,ChartMouseOverEvent, ChartMouseOutEvent } from 'ng2-google-charts';

export interface roleNlayout {
  id: string;
  desc: string;
  checked:boolean
}

export interface UserData {
  layoutDesc: string; //1
  docNameDesc:string; //1
  fieldName:string;
  docViewName:string; //1
  sectionName: number; //1
  fieldLabel: string; //1
  scale: number; //1
  validationName: string; //1
  widgetName: string; //1
  validationOverride: string; //1
  showDesc: string; // 1 name to change
  enableAutoSugg: string; //1 
  displayLength: string; //1 
  required: string; //1
  showLookupUrl: string; //1
  bgColor: string; //1
  fgColor: string; //1
  customValidator: string //1
}

export interface ConfigData {
  'description':string;
   'propValue':string;
  // 'datatype':string;
  // 'factoryName':string;
  // 'maxlength':string;
  // 'encrypted':string;
  // 'comments':string;
  // 'docIdList':string;
  // 'funcUrl1':string;
  // 'funcUrl2':string;
  // 'categoryId':string;
  // 'levelId':string;
  // 'position':string;
  'modifyTs':string
}



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    Animations.animeTrigger
  ]
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[] = [
  'layoutDesc',
  'docNameDesc',
  'docViewName',
  'sectionName',
  'fieldName',
  'fieldLabel',
  'scale',
  'validationName',
  'widgetName',
  'validationOverride',
  'showDesc',
  'enableAutoSugg',
  'displayLength',
  'required',
  'showLookupUrl',
  'bgColor',
  'fgColor',
  'customValidator'
  ];

  displayedConfigColumns: string[] = [
    'description',
    'propValue',
  // 'datatype',
  // 'factoryName',
  // 'maxlength',
  // 'encrypted',
  
  // 'comments',
  // 'docIdList',
  // 'funcUrl1',
  // 'funcUrl2',
  // 'categoryId',
  // 'levelId',
  // 'position',
  'modifyTs',
  ];

  dataSource: MatTableDataSource<UserData>;
  // @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  // @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSourceConfig: MatTableDataSource<ConfigData>;
  // @ViewChild(MatPaginator, { static: true }) paginatorConfig: MatPaginator;
  // @ViewChild(MatSort, { static: true }) sortConfig: MatSort;

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  
  layoutLabel: Label = new Label();
  show: string = 'close';

  
  
  
  
  dataLists: any;
  mainLists: roleNlayout[] = [];
  roleLists: roleNlayout[] = [];
  layoutLists: roleNlayout[] = [];
  roleListsCopy: any;
  dateRange:string = 'Last 5 Migration';
  fromDate:Date = null;
  toDate:Date = null;
  Hideflag:boolean = true;
  moduleType:string = 'Layout';
  showNodataLayout:boolean = false;
  showNodataAppConfig:boolean = false;

  constructor(private adminService: AdminService, private Activrouter: ActivatedRoute, public dialog?: MatDialog) {
    this.Activrouter.url.subscribe(url => {
      if(url.length) {
        this.adminService.changeMessage(url[0].path);
      } else {
        this.adminService.changeMessage('dashboard');
      }
      
      
    });

    this.getReports();

  }

  ngOnInit() {

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilterAppConfig(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceConfig.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceConfig.paginator) {
      this.dataSourceConfig.paginator.firstPage();
    }
  }

  ModuleFilter(flag: number): void {

    const dialogRef = this.dialog.open(DashboardFilter, {
      data: { matSelectIndex: flag, dateRange: this.dateRange, mainLists: this.mainLists, fromDate:this.fromDate, toDate:this.toDate, moduleType:this.moduleType },
      width: '400px',
      panelClass: 'custom-modalbox'
    });
    dialogRef.afterClosed().subscribe(result => {
      let data: any = result
      if (data.length > 0) {
       if(data == 'Config App') {
        this.moduleType = data;
       } else if (data == 'Layout') {
        this.moduleType = data;
       }
      }
    });
  }

  getReports() {

    this.adminService.getMigrationReport().subscribe(res => {

      const layout = res.listOflayoutReportDetails;
      const config = res.listOfAppConfigDetails;
      // Assign the data to the data source for the table to render
      //console.log(users);
      this.dataSource = new MatTableDataSource(layout);
      if(typeof this.dataSource.filteredData == 'object' && this.dataSource.filteredData.length == 0) {
        this.showNodataLayout = true;
      } else if(this.dataSource.filteredData == null) {
        this.showNodataLayout = true;
      }
      // this.dataSource.paginator = this.paginator;
      // this.dataSource.sort = this.sort;

      
      /**config report assign */
      this.dataSourceConfig = new MatTableDataSource(config);

      if(typeof this.dataSourceConfig.filteredData == 'object' && this.dataSourceConfig.filteredData.length == 0) {
        this.showNodataAppConfig = true;
      } else if(this.dataSourceConfig.filteredData == null) {
        this.showNodataAppConfig = true;
      }

      // this.dataSourceConfig.paginator = this.paginatorConfig;
      // this.dataSourceConfig.sort = this.sortConfig;
      this.dataSource.paginator = this.paginator.toArray()[0];
      this.dataSource.sort = this.sort.toArray()[0];
      this.dataSourceConfig.paginator = this.paginator.toArray()[1];
      this.dataSourceConfig.sort = this.sort.toArray()[1];
      this.Hideflag = false;
    })
  }
}


@Component({
  selector: 'dashboard-filter',
  templateUrl: 'dashboard-filter.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardFilter implements OnInit {

  //moduleType: string = 'Layout';
  constructor(public dialogRef: MatDialogRef<DashboardFilter>, private adminService: AdminService, @Inject(MAT_DIALOG_DATA) public data) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    


  }

}
