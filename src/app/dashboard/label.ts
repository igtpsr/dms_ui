export class Label {
    constructor() {

    }
    layoutLable: any = [
        'Layout Desc',
        'Doc Name Desc',
        'Doc View',
        'Section ',
        'Field Name',
        'Field Label',
        'Scale',
        'Validation',
        'Widget',
        'Validation Override',
        'Show Desc',
        'Enable Auto Sugg',
        'Display Length',
        'Required',
        'Show Lookup Url',
        'Bg Color',
        'Fg Color',
        'Custom Validator'
    ]

    configAppLable: any = [
        'Property',
        'Value',
        // 'Data Type',
        // 'Factory Name',
        // 'Max Length',
        // 'Encrypted',
        
        // 'Comments',
        // 'Doc Id List',
        // 'Function Url 1',
        // 'Function Url 2',
        // 'Category Id',
        // 'Level Id',
        // 'Position',
        'Modify TS'

    ]
}