import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
//import { Http, RequestOptions, Headers } from '@angular/common/http';
import { Observable, BehaviorSubject, from } from "rxjs";
import { AdminDetail } from "../classes/admin-detail";
import { Router } from "@angular/router";

import { StorageService } from "../secret/storage.service";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../environments/environment";

interface status {
  Status: string;
}

interface logoutStatus {
  message: string;
}

@Injectable({
  providedIn: "root",
})
export class AdminService {
  // Base URL
  private baseUrl = environment.baseUrl;
  private migrationUrl = environment.migrationUrl;

  private message = new BehaviorSubject("Main");
  currentMessage = this.message.asObservable();
  changeMessage(message: any) {
    this.message.next(message);
  }

  private showLoader = new BehaviorSubject("false");
  currentLoader = this.showLoader.asObservable();
  changeLoaderStatus(message: string) {
    this.showLoader.next(message);
  }

  startLoader() {
    this.changeLoaderStatus("true");
  }

  endLoader() {
    setTimeout(() => {
      this.changeLoaderStatus("false");
    }, 100);
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    private storageService: StorageService,
    private toastr: ToastrService,

  ) { }

  saveAdminDetails(adminDetail: AdminDetail): Observable<any> {
    let url = this.baseUrl + "saveAdmin";
    return this.http.post(url, adminDetail);
  }

  login(adminDetail: AdminDetail): Observable<any> {
    let url = this.baseUrl + "brimLogin.do";
    return this.http.post(url, adminDetail, { observe: "response" as "body" })
  }

  logout() {
    let url = this.baseUrl + "logout.do";
    // Remove the token from the localStorage.
    // this.http.put(url).subscribe(response => {
    //   if (response == 1) {
    //     //alert('Successfully Logut');
    //     localStorage.removeItem('token');
    //     localStorage.removeItem('id');
    //     this.router.navigate(['/login']);
    //   }
    // }, error => {
    //   alert('Error in logout');
    //   console.log(error);
    // });

    // let url = this.migrationUrl + "updateTheClientDbDetails";
    let token = this.storageService.secureStorage.getItem("token");
    let obj = { token: token };
    return this.http
      .put<logoutStatus>(url, obj)
      .subscribe(
        (res) => {
          if (res.message.toUpperCase() == "SUCCESS") {
            this.storageService.secureStorage.clear();
            this.router.navigate(["/login"]);
          } else {
            this.toastr.error("Something Went Wrong, Please contact to admin");
          }
        },
        (error) => {
          this.storageService.secureStorage.clear();
          this.router.navigate(["/login"]);
        }
      );
  }

  changeLog() {
    this.router.navigate(["/changelog"]);
  }
  /*
   * Check whether User is loggedIn or not.
   */

  isLoggedIn(): boolean {
    return !!this.storageService.secureStorage.getItem("token");
    // create an instance of JwtHelper class.
    //let jwtHelper = new JwtHelperService();

    // get the token from the localStorage as we have to work on this token.
    // let token = localStorage.getItem('token');

    // // check whether if token have something or it is null.
    // if (!token) {
    //   return false;
    // }

    // // get the Expiration date of the token by calling getTokenExpirationDate(String) method of JwtHelper class. this method accept a string value which is nothing but token.

    // if (token) {
    //   let expirationDate = jwtHelper.getTokenExpirationDate(token);

    //   // check whether the token is expired or not by calling isTokenExpired() method of JwtHelper class.

    //   let isExpired = jwtHelper.isTokenExpired(token);

    //   return !isExpired;
    // }
  }

  getAdminDetail(adminId): Observable<any> {
    let url = this.baseUrl + "getAdminData/" + adminId;
    return this.http.get(url);
  }

  saveEnvironment(environmentData: any): Observable<any> {
    let url = this.migrationUrl + "createTheClientDbDetails";
    return this.http.post(url, environmentData);
  }

  updateEnvironment(environmentData: any): Observable<any> {
    let url = this.migrationUrl + "updateTheClientDbDetails";
    return this.http.put(url, environmentData);
  }

  getEnvironment(): Observable<any> {
    let url = this.migrationUrl + "getTheConfigurationAppDetails";
    return this.http.get<any>(url);
  }

  callSecurityMigrate(migrationData: any, sourceDb: string, destinationDb: string): Observable<any> {
    let url = this.migrationUrl + "migrateTheSecurityModule";
    let token = this.storageService.secureStorage.getItem("token");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token,
      sourceEnvironmentType: sourceDb.toUpperCase(),
      destinationEnvironmentType: destinationDb.toUpperCase(),
      skip: 'true'
    });
    return this.http.put(url, migrationData, { headers });
  }

  callLayoutMigrate(migrationData: any, sourceDb: string, destinationDb: string): Observable<any> {
    let url = this.migrationUrl + "migrateTheLayoutModule";
    let token = this.storageService.secureStorage.getItem("token");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token,
      sourceEnvironmentType: sourceDb.toUpperCase(),
      destinationEnvironmentType: destinationDb.toUpperCase(),
      skip: 'true'
    });
    return this.http.put(url, migrationData, { headers });
  }

  callAppConfigMigrate(migrationData: any, sourceDb: string, destinationDb: string): Observable<any> {
    let url = this.migrationUrl + "migrateTheAppConfigModule";
    let token = this.storageService.secureStorage.getItem("token");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token,
      sourceEnvironmentType: sourceDb.toUpperCase(),
      destinationEnvironmentType: destinationDb.toUpperCase(),
      skip: 'true'
    });
    return this.http.put(url, migrationData, { headers });
  }

  customRulesReportMigrate(migrationData: any, sourceDb: string, destinationDb): Observable<any> {
    let url = this.migrationUrl + "migrateTheAdhocDocuments";
    let token = this.storageService.secureStorage.getItem("token");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token,
      sourceEnvironmentType: sourceDb.toUpperCase(),
      destinationEnvironmentType: destinationDb.toUpperCase(),
      skip: 'true'
    });
    return this.http.put(url, migrationData, { headers });
  }

  rulesMigrate(migrationData: any, sourceDb: string, destinationDb): Observable<any> {
    let url = this.migrationUrl + "migrateTheRules";
    let token = this.storageService.secureStorage.getItem("token");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token,
      sourceEnvironmentType: sourceDb.toUpperCase(),
      destinationEnvironmentType: destinationDb.toUpperCase(),
      skip: 'true'
    });
    return this.http.put(url, migrationData, { headers });
  }

  getAllTheIdsAndRuleFiles(sourceDb:string, moduleType:string): Observable<any> {
    let destinationDb: string = '';
    let token = this.storageService.secureStorage.getItem("token");
    const headers = new HttpHeaders({
      Authorization: "Bearer " + token,
      sourceEnvironmentType: sourceDb.toUpperCase(),
      destinationEnvironmentType: destinationDb.toUpperCase(),
      skip: 'true'
    });
    let url = this.migrationUrl + "getAllTheIdsAndRuleFiles/"+moduleType;
    return this.http.get(url, { headers });
  }

  getMigrationReport(): Observable<any> {
  
    let url = this.migrationUrl + "getTheDashboardReports";
    return this.http.get<any>(url);
  }
  getAdhocDocumentsList(): Observable<any> {
    let url = this.migrationUrl + "getTheListOfAdhocDocumentsNames";
    return this.http.get<Array<string>>(url);
  }

  getTheLastMigratedTimePeriod(): Observable<any> {
    let url = this.migrationUrl + "getTheLastMigratedTimePeriod";
    return this.http.get(url)
  }
  removeTheDBEnvironmentDetails(id): Observable<status> {
    let url = this.migrationUrl + "removeTheDBEnvironmentDetails/" + id;
    return this.http.delete<status>(url);
  }

  testTheDBConnection(environmentData: object) {
    let url = this.migrationUrl + "testTheDBConnection";
    return this.http.post<status>(url, environmentData);
  }

  updateTheExpiryTimeOfToken() {
    var token = this.storageService.secureStorage.getItem("token");
    return this.http.put(this.baseUrl + "updateTheExpiryTimeOfToken", {
      token: token,
    });
  }

  getToken() {
    if (!!this.storageService.secureStorage.getItem("token")) {
      return this.storageService.secureStorage.getItem("token");
    } else {
      return null;
    }

  }
  getAllUserProfile(): Observable<any> {
    let url = this.baseUrl + "getAllUserProfile";
    return this.http.get(url)
  }
  createUser(userlistObj: any): Observable<any> {
    let url = this.baseUrl + "createUserProfile";
    return this.http.post(url, userlistObj);
  }

  updateUser(userlistObj: any): Observable<any> {
    let url = this.baseUrl + "updateUserProfile";
    return this.http.put(url, userlistObj);
  }

  getAllRoleProfile(): Observable<any> {
    let url = this.baseUrl + "getAllRoleProfile";
    return this.http.get(url)
  }

  createRoleProfile(rolelistObj: any): Observable<any> {
    let url = this.baseUrl + "createRoleProfile";
    return this.http.post(url, rolelistObj);
  }

  updateRoleProfile(rolelistObj: any): Observable<any> {
    let url = this.baseUrl + "updateRoleProfile";
    return this.http.put(url, rolelistObj);
  }

  getTheDashBoardChartDetails(): Observable<any> {
    let url = this.migrationUrl + "getTheDashBoardChartDetails";
    return this.http.get<any>(url)
  }
  restoreDestinationDB(moduleName): Observable<any> {
    let url = this.migrationUrl + "restoreTheDataByModule/"+moduleName;
    return this.http.put(url, null);
  }
}
