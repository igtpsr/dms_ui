import { StorageService } from './../secret/storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  commonURL = 'http://localhost:8080/authApi';
  private userLoggedIn = new Subject<boolean>();

  constructor(private http: HttpClient, private storage: StorageService) {
    this.userLoggedIn.next(false);
  }

  doLogin(username: string, password: string) {
    this.setUserLoggedIn(true);
    return this.http.post(this.commonURL + '/brimLogin.do', {
      userName: username,
      password: password,
    });
  }

  updateTheExpiryTimeOfToken() {
    var token = this.storage.secureStorage.getItem('token');
    return this.http.put(this.commonURL + '/updateTheExpiryTimeOfToken', {
      token: token,
    });
  }

  doLogout() {
    var token = this.storage.secureStorage.getItem('token');
    return this.http.put(this.commonURL + '/logout.do', {
      token: token,
    });
  }

  setUserLoggedIn(userLoggedIn: boolean) {
    this.userLoggedIn.next(userLoggedIn);
  }

  getUserLoggedIn(): Observable<boolean> {
    return this.userLoggedIn.asObservable();
  }
}
