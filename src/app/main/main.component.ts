import { Label } from './../dashboard/label';
import { Component, OnInit, Output, Input, Inject, ViewChildren, QueryList } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Animations } from '../animation';
import { FormGroup, Validators, FormControl, FormArray, ValidatorFn } from '@angular/forms';
import * as _ from "lodash";
import { AdminService } from '../services/admin.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { StorageService } from '../secret/storage.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from "../../environments/environment";

export interface DialogData {
  moduletype: string;
  sourceDb: string;
  destinationDb: string;
}
export interface roleNlayout {
  id: string;
  desc: string;
  checked:boolean
}

export interface appConfig {
  id: string;
  desc: string;
  checked:boolean
}

export interface datePicker {
  fromDate:Date;
  toDate:Date
}
export interface dbenvironments {
  name:string;
  priority:number
}

export interface MigrationParams {
  listOfRoleId: [{ roleId: number; roleName: string }];
  isMigrateAllSecurityRoleId: boolean;
  listOfLayoutId: [{ layoutId: number; layouDesc: string }];
  isMigrateAllLayoutId: boolean;
  fromDate:any;
  toDate:any;
  moduleName:string,
  clientSpec:string
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    Animations.animeTrigger
  ]
})
export class MainComponent implements OnInit {

  showFlag: boolean = false;
  sourceDb: string;
  destinationDb: string;
  moduleType: string = 'Security';
  isChecked;
  isCheckedDesc;

  // dbenvironments:dbenvironments[] = [
  //   {
  //     name:'Prod',
  //     priority:4
  //   },
  //   {
  //     name:'Test',
  //     priority:3
  //   },
  //   {
  //     name:'QA',
  //     priority:2
  //   },
  //   {
  //     name:'Dev',
  //     priority:1
  //   }
  // ]

  show: string = 'close';
  roleLists: roleNlayout[] = [];
  layoutLists: roleNlayout[] = [];
  roleListsCopy: any;
  searchTerm: string;
  idType: string = 'Role Id';
  dataLists: any;
  mainLists: roleNlayout[] = [];
  @ViewChildren('checkBox') checkBox: QueryList<any>;
  customLists:  roleNlayout[] = [];;
  customListsCopy: any;
  rulesLists:  roleNlayout[] = [];;
  rulesListsCopy: any;
  timestamp:any;
  fromDate:Date;
  toDate:Date;
  mainListsStatus:boolean = false;
  envDatalists:any[] = [];
  unSortedEnvList:any[] = [];
  appConfigLists: appConfig[] = [];

  existingSortedEnvList = ['DEV','TEST','QA','PROD'];


  constructor(private adminService: AdminService, private toastr?: ToastrService, private Activrouter?: ActivatedRoute, public dialog?: MatDialog, public _snackBar?: MatSnackBar,  private storageService?: StorageService) {

    this.Activrouter.url.subscribe(url => {
      this.adminService.changeMessage(url[0].path);
    });

    //this.adminService.startLoader();

  }

  ngOnInit() {

    // this.adminService.getAdhocDocumentsList().subscribe(
    //   response => {
    //     for (let i = 0; i < response.length; i++) {
    //       this.customLists[i] = {
    //         id: response[i],
    //         desc: response[i],
    //         checked:false
    //       };
    //     }
    //      this.customListsCopy = this.customLists;
    //    },
    //    error => {
    //        console.log(error);
    //    },

    // );


    this.getEnvironmentList();

    this.getMigrationTimeStamp();

  }

  search(): void {
    let term = this.searchTerm;
    this.mainLists = this.roleListsCopy.filter(function (tag) {
       return tag.desc.indexOf(term.toUpperCase()) >= 0 || tag.id.indexOf(term) >= 0;
    });
  }

  sortAscToDesc(objList:roleNlayout[] = []) {
      return  objList.sort((a,b) => (a.desc > b.desc) ? 1 : ((b.desc > a.desc) ? -1 : 0));
  }


  getMigrationTimeStamp() {
    this.adminService.getTheLastMigratedTimePeriod().subscribe(res=>{
      let value:Date = new Date(res.lastMigratedTime);
      if(moment(value).format("MM-DD-YYYY") == (moment(new Date()).format("MM-DD-YYYY"))) {
        this.timestamp = value.toLocaleTimeString();
      } else {
        this.timestamp = moment(value).format("MM-DD-YYYY");
      }
    })
  }

  selectAppConfig(){
    this.mainListsStatus = true;
    this.isChecked = false;
    this.appConfigLists[0] = {
      id: 'Client Spec',
      desc: 'Client Spec',
      checked:false
    };
    this.appConfigLists[1] = {
      id: 'All Property',
      desc: 'All Property',
      checked:false
    };
    this.mainLists = this.appConfigLists;
    this.roleListsCopy = this.mainLists;
    this.mainListsStatus = false;
  }

  onChange(item){
    if(this.moduleType === 'App Config'){
       this.isChecked = !this.isChecked;
       this.isCheckedDesc = item.desc;
    }
  }

  selectedsourceDb() {

    this.dataLists = [];
    this.mainLists = [];
    this.roleLists = [];
    this.layoutLists = [];
    this.roleListsCopy = [];
    this.appConfigLists = [];
    this.mainListsStatus = true;

    //this.adminService.startLoader();
    this.adminService.getAllTheIdsAndRuleFiles(this.sourceDb, this.moduleType).subscribe(res => {
      console.log('Data size :', res.listOfReports.length);
      this.dataLists = res;
      this.mainListsStatus = false;

      if(this.moduleType == 'Security' || this.moduleType == 'Layout') {
          this.setRoleNLayoutIds();
      } else {
        this.setCustomReportsNRules();
      }
      //this.selectedModelType();
    });

  }

  setRoleNLayoutIds() {

    if (this.moduleType == 'Security') {
      for (let i = 0; i < this.dataLists.listOfRoleId.length; i++) {
        this.roleLists[i] = {
          id: this.dataLists.listOfRoleId[i].roleId+'',
          desc: this.dataLists.listOfRoleId[i].roleName,
          checked:false
        };
      }
      this.mainLists =  this.sortAscToDesc(this.roleLists);
      this.roleListsCopy = this.mainLists;

    } else if (this.moduleType == 'Layout') {

      for (let i = 0; i < this.dataLists.listOfLayoutId.length; i++) {
        this.layoutLists[i] = {
          id: this.dataLists.listOfLayoutId[i].layoutId+'',
          desc: this.dataLists.listOfLayoutId[i].layouDesc,
          checked:false
        };
      }

      this.mainLists = this.sortAscToDesc(this.layoutLists);
      this.roleListsCopy = this.mainLists;

    }



  }

  setCustomReportsNRules() {

    if (this.moduleType == 'CustomRules') {

      for (let i = 0; i < this.dataLists.listOfCustomRules.length; i++) {
        this.customLists[i] = {
          id: this.dataLists.listOfCustomRules[i],
          desc: this.dataLists.listOfCustomRules[i],
          checked:false
        };
      }

      this.mainLists =  this.sortAscToDesc(this.customLists);
      this.roleListsCopy = this.mainLists;

    } else if(this.moduleType == 'Rules'){

      for (let i = 0; i < this.dataLists.listOfRules.length; i++) {
        this.rulesLists[i] = {
          id: this.dataLists.listOfRules[i],
          desc: this.dataLists.listOfRules[i],
          checked:false
        };
      }

      this.mainLists =  this.sortAscToDesc(this.rulesLists);
      this.roleListsCopy = this.mainLists;

    } else if(this.moduleType == 'Reports'){

      for (let i = 0; i < this.dataLists.listOfReports.length; i++) {
        this.rulesLists[i] = {
          id: this.dataLists.listOfReports[i],
          desc: this.dataLists.listOfReports[i],
          checked:false
        };
      }

      this.mainLists =  this.sortAscToDesc(this.rulesLists);
      this.roleListsCopy = this.mainLists;

    }




  }

  selectedModelType() {
   // console.log('module type ',this.moduleType);
    this.adminService.startLoader();
    //this.mainLists.forEach(item => item.checked = false);

    this.dataLists = [];
    this.mainLists = [];
    this.roleLists = [];
    this.layoutLists = [];
    this.roleListsCopy = [];
    this.customLists = [];
    this.rulesLists = [];
    //this.mainListsStatus = true;

    this.sourceDb = '';
    this.destinationDb = '';
    if (this.moduleType == 'Security') {
      this.idType = 'Role Id'
    } else if (this.moduleType == 'Layout') {
      this.idType = 'Layout Id';
    } else if (this.moduleType == 'CustomRules') {
      this.idType = 'File Names';
    } else if(this.moduleType == 'Rules') {
      this.idType = 'File Names';
    } else if (this.moduleType == 'Reports') {
      this.idType = 'File Names';
    } else if (this.moduleType == 'App Config') {
      this.idType = 'File Names';
    }
    this.adminService.endLoader();
  }

  callsecurityModule() {

    let checkedList: any = [];
    const checked = this.mainLists.filter(checkbox => checkbox.checked);
    checked.forEach(data => {
      checkedList.push({
        roleId: +data.id.trim(),
        roleName: data.desc.trim()
      })
    })

    let migrationData: MigrationParams;
    migrationData = {
      listOfRoleId: checkedList,
      isMigrateAllSecurityRoleId:false,
      listOfLayoutId: null,
      isMigrateAllLayoutId: false,
      fromDate: null,
      toDate: null,
      moduleName: this.moduleType.toUpperCase(),
      clientSpec:null
    }
    this.storageService.secureStorage.setItem("migartionStatus",true);
    this.adminService.callSecurityMigrate(migrationData, this.sourceDb, this.destinationDb).subscribe(res => {
      let resData: any = res;
      this.getMigrationTimeStamp();
      this.storageService.secureStorage.setItem("migartionStatus",false);
      this.dialog.closeAll();
      if (resData.Status == 'Success') {
        this.toastr.success('Security Successfully Migrated','',{ tapToDismiss: true , disableTimeOut:true});
      } else {
        this.toastr.error(resData.Status,'',{ tapToDismiss: true , disableTimeOut:true});
      }
    });

  }

  callLayoutModule() {

    let checkedList: any = [];
    const checked = this.mainLists.filter(checkbox => checkbox.checked);
    checked.forEach(data => {
      checkedList.push({
        layoutId: +data.id.trim(),
        layouDesc: data.desc.trim()
      })
    })

    let migrationData: MigrationParams;
    migrationData = {
      listOfRoleId: null,
      isMigrateAllSecurityRoleId:false,
      listOfLayoutId: checkedList,
      isMigrateAllLayoutId: false,
      fromDate: moment(this.fromDate).format("YYYY-MM-DDT00:00:00.000"),
      toDate:  moment(this.toDate).format("YYYY-MM-DDT23:59:59.000"),
      moduleName: this.moduleType.toUpperCase(),
      clientSpec:null
    }
    this.storageService.secureStorage.setItem("migartionStatus",true);
    this.adminService.callLayoutMigrate(migrationData, this.sourceDb, this.destinationDb).subscribe(res => {
      this.storageService.secureStorage.setItem("migartionStatus",false);
      let resData: any = res;
      this.getMigrationTimeStamp();
      this.dialog.closeAll();
      if (resData.Status == 'Success') {
        this.toastr.success('Layout Successfully Migrated','',{ tapToDismiss: true , disableTimeOut:true});
      } else {
        this.toastr.error(resData.Status,'',{ tapToDismiss: true , disableTimeOut:true});
      }
    });

  }

  callAppConfigModule() {

    let checkedList: any = [];
    const checked = this.checkBox.filter(checkbox => checkbox.checked);
    checked.forEach(data => {
      checkedList.push(data.value.trim())
    });

    let clientSpec;

    if(checkedList == 'Client Spec')
      clientSpec = checkedList[0];
    else
      clientSpec = null;

    let migrationData: MigrationParams;
    migrationData = {
      listOfRoleId: null,
      isMigrateAllSecurityRoleId:false,
      listOfLayoutId: null,
      isMigrateAllLayoutId: false,
      fromDate: moment(this.fromDate).format("YYYY-MM-DDT00:00:00.000"),
      toDate:  moment(this.toDate).format("YYYY-MM-DDT23:59:59.000"),
      moduleName: this.moduleType.toUpperCase(),
      clientSpec: clientSpec
    }
    this.storageService.secureStorage.setItem("migartionStatus",true);
    this.adminService.callAppConfigMigrate(migrationData, this.sourceDb, this.destinationDb).subscribe(res => {
      this.storageService.secureStorage.setItem("migartionStatus",false);
      let resData: any = res;
      this.getMigrationTimeStamp();
      this.dialog.closeAll();
      if (resData.Status == 'Success') {
        this.toastr.success('App Config Successfully Migrated','',{ tapToDismiss: true , disableTimeOut:true});
      } else {
        this.toastr.error(resData.Status,'',{ tapToDismiss: true , disableTimeOut:true});
      }
    });

  }

  customRulesReportModule() {
    let checkedList: any = [];
    const checked = this.checkBox.filter(checkbox => checkbox.checked);
    checked.forEach(data => {
      checkedList.push(data.value.trim())
    })
    this.storageService.secureStorage.setItem("migartionStatus",true);
    this.adminService.customRulesReportMigrate(checkedList, this.sourceDb, this.destinationDb).subscribe(res => {
      this.storageService.secureStorage.setItem("migartionStatus",false);
      let resData: any = res;
      this.getMigrationTimeStamp();
      this.dialog.closeAll();
      if (resData.Status == 'Success') {
          if(this.moduleType === 'CustomRules')
          this.toastr.success('Custom Rules Successfully Migrated','',{ tapToDismiss: true , disableTimeOut:true});
          else
          this.toastr.success('Reports Successfully Migrated','',{ tapToDismiss: true , disableTimeOut:true});
      } else {
        this.toastr.error(resData.Status,'',{ tapToDismiss: true , disableTimeOut:true});
      }
    });

  }

  rulesModule() {
    let checkedList: any = [];
    const checked = this.checkBox.filter(checkbox => checkbox.checked);
    checked.forEach(data => {
      checkedList.push(data.value.trim())
    })
    this.storageService.secureStorage.setItem("migartionStatus",true);
    this.adminService.rulesMigrate(checkedList, this.sourceDb, this.destinationDb).subscribe(res => {
      this.storageService.secureStorage.setItem("migartionStatus",false);
      let resData: any = res;
      this.getMigrationTimeStamp();
      this.dialog.closeAll();
      if (resData.Status == 'Success') {
        this.toastr.success('Rules Successfully Migrated','',{ tapToDismiss: true , disableTimeOut:true});
      } else {
        this.toastr.error(resData.Status,'',{ tapToDismiss: true , disableTimeOut:true});
      }
    });

  }



  resetForm() {
    this.mainLists.forEach(item => item.checked = false);
    this.sourceDb = '';
    this.destinationDb = '';

  }

  validateMigrate() {

    let checkedList: any = [];
    const checked = this.checkBox.filter(checkbox => checkbox.checked);
    checked.forEach(data => {
      checkedList.push(data.value.trim())
    });

    console.log(checkedList.length);

    if(this.validateForm()) {
      return;
    }

    if(this.moduleType == 'Layout'||(this.moduleType == 'App Config' && checkedList[0] == 'All Property') ) {
      this.chooseDate();
    } else {
      this.submitMigration()
    }


  }

  submitMigration():void {

    const dialogRef = this.dialog.open(ConfirmMigrateDialog, {
      width: '380px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.startProgress();
        if(this.moduleType == 'Security') {
          this.callsecurityModule();
        }  else if(this.moduleType == 'Layout') {
          this.callLayoutModule();
        } else if(this.moduleType == 'App Config') {
          this.callAppConfigModule();
        } else if(this.moduleType == 'CustomRules' || this.moduleType=='Reports')  {
          this.customRulesReportModule();
        } else {
          this.rulesModule();
        }


      }
    });
  }

  startProgress(): void {
    const progressRef = this.dialog.open(ProgressDialog, {
      width: '380px',
      disableClose: true
    });
    progressRef.afterClosed().subscribe(result => {
      if (result) {
        this.warSnackBar('Migration cancelled');
      }
    })
  }

  startRestoreProgress(): void {
    const progressRef = this.dialog.open(RestoreProgressDialog, {
      width: '380px',
      disableClose: true
    });
    progressRef.afterClosed().subscribe(result => {
      if (result) {
        this.warSnackBar('Migration cancelled');
      }
    })
  }

  chooseDate() {
    const progressRef = this.dialog.open(DatePickerDialog, {
      width: '530px',
      disableClose: true,
      data:{fromDate:null, toDate:null},

    });
    progressRef.afterClosed().subscribe(result => {
      if(result != '') {
        if (result.fromDate != null && result.toDate != null) {
          this.fromDate = result.fromDate;
          this.toDate = result.toDate;
          this.submitMigration();
        } else {
          this.warSnackBar('Both Dates Are Mandatory');
        }
      }

    })
    return false;

  }

  warSnackBar(msg:string): void {
    this._snackBar.open(msg, '', {
      duration: 5000,
      panelClass: 'mat-warn-n-w',
      verticalPosition: 'bottom',
      horizontalPosition: 'left',

    });
  }

  validateForm():boolean {

    const listChecked = this.moduleType != 'App Config' ? this.mainLists.filter(checkbox => checkbox.checked): [{}];
    const sourceObj = this.envDatalists.filter(item=>item.dbEnvironmentType==this.sourceDb);
    const destObj = this.envDatalists.filter(item=>item.dbEnvironmentType==this.destinationDb);

    if(listChecked.length == 0 || sourceObj.length == 0 || destObj.length == 0) {
      this.warSnackBar('Please Fill Mandatory Fields');
      return true;
    } else if(sourceObj[0].priority>=destObj[0].priority) {
      this.warSnackBar('Source and Destination DB selection is Invalid');
      return true;
    }
     return false;
  }

  getEnvironmentList() {
    this.adminService.startLoader();
   // this.envDatalists = [];
   let tempList = [];
    this.adminService.getEnvironment().subscribe(res => {
      this.unSortedEnvList =  res.filter(res=>{
       return res.isThisDbEnvVerified===1;
      })
      tempList = new Array(this.unSortedEnvList.length);
      /*This code will add the environment sequntially, which is already given in "existingSortedEnvList" array*/
      /*but this code has some loop fall, so outcome this loop fall we are writing below code */

      this.unSortedEnvList.forEach(data=>{
        var index = this.existingSortedEnvList.indexOf(data.dbEnvironmentType);
        tempList[index] = data;
      });

      /*When this code is needfull..?
      Ans: When user is added db details or db environment in Configuration App Page and not verified connection details
      then that time because of line number 541 condition, the db environment will not add in array
      */

      /*This loop is checking for empty index values, if index values is
      empty that time it will not add in envDatalists array
      */
      tempList.forEach(data=>{
        if(data){
          this.envDatalists.push(data);
        }
      })
      console.log('Data ',this.envDatalists);
      this.adminService.endLoader();
    })
  }
  restore(){
    console.log(this.moduleType);
    const dialogRef = this.dialog.open(ConfirmRestoreDialog, {
      width: '380px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.adminService.startLoader();
        this.startRestoreProgress();
        this.adminService.restoreDestinationDB(this.moduleType.toUpperCase()).subscribe(res=>{
          this.toastr.success('Successfully Restored','',{ tapToDismiss: true , disableTimeOut:true});
          this.adminService.endLoader();
          this.dialog.closeAll();
        })
      }
    });
  }

}

@Component({
  selector: 'confirm-migrate-dialog',
  templateUrl: 'confirm-migrate-dialog.html'
})
export class ConfirmMigrateDialog {
  constructor(public dialogRef: MatDialogRef<ConfirmMigrateDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'confirm-restore-dialog',
  templateUrl: 'confirm-restore-dialog.html'
})
export class ConfirmRestoreDialog {
  constructor(public dialogRef: MatDialogRef<ConfirmRestoreDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'progress-dialog',
  templateUrl: 'progress-dialog.html'
})
export class ProgressDialog {
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  completionStatus:string = '0';
  constructor(public dialogRef: MatDialogRef<ProgressDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.connect();
  }

    //  Web socket with sockJS for realtime response
    socket: any;
    stompClient: any;
    ws: any;

    connect() {
      //  console.log()
      //connect to stomp where stomp endpoint is exposed
      this.socket = new SockJS(environment.baseUrl + "brimSocket", {});
      this.ws = Stomp.over(this.socket);
      this.stompClient = this.ws;
      this.stompClient.connect(
        {},
        (success) => this.connectSuccessCallback(),
        (error) => this.connectErrorCallback(error)
      );
    }
    // successfull socket connect callback function
    connectSuccessCallback() {
      this.stompClient.subscribe('/queue/percentage/', (res) => {
        // let response:any = JSON.stringify(res);
        // let percP:any = JSON.parse(response);
        // this.completionStatus = JSON.parse(percP.body).percentage;
        this.completionStatus = res.body;
      });
      this.stompClient.subscribe('/queue/percentage/others/', (res) => {
        this.completionStatus = res.body;
      });
    }
    // Error while connecting webscoket connection callback
    static counter: number = 0;
    connectErrorCallback(error) {
      console.log(' SockJS error', error);
    }

}

@Component({
  selector: 'restore-progress-dialog',
  templateUrl: 'restore-progress-dialog.html'
})
export class RestoreProgressDialog {
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  completionStatus:string = '0';
  constructor(public dialogRef: MatDialogRef<RestoreProgressDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'date-picker-dialog',
  templateUrl: 'date-picker-dialog.html'
})
export class DatePickerDialog {
  tomorrow = new Date();
  constructor(public dialogRef: MatDialogRef<ConfirmMigrateDialog>, @Inject(MAT_DIALOG_DATA) public data: datePicker) {
    this.tomorrow.setDate(this.tomorrow.getDate());
   }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
