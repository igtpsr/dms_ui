import { IdleService } from "./../../services/idle.service";
import { Component, OnInit } from "@angular/core";
import { AdminService } from "../../services/admin.service";
import { ProgressBarMode } from "@angular/material/progress-bar";
import { trigger, style, animate, transition } from "@angular/animations";
import { StorageService } from 'src/app/secret/storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
  animations: [
    trigger("enterAnimation", [
      transition(":leave", [style({}), animate(500, style({ opacity: 0 }))]),
    ]),
  ],
})
export class HeaderComponent implements OnInit {
  mode: ProgressBarMode = "indeterminate";
  rimDoc: String =  environment.rimDocs;


  constructor(
    private admin?: AdminService,
    private idleService?: IdleService,
   private storageService?: StorageService
  ) {}
  breadScrum: string;
  loaderStatus: string;
  flag:boolean;
  userName:string =  this.storageService.secureStorage.getItem("userName");
  ngOnInit() {
    this.admin.currentMessage.subscribe((res) => {
      if(res == 'role Profile') {
        this.flag = true;
      } else {
        this.flag = false;
      }
      this.breadScrum = res;
    });
    this.admin.currentLoader.subscribe((res) => {
      this.loaderStatus = res;
      // console.log(this.loaderStatus);
    });
  }

  logout(): void {
    this.admin.logout();
    this.idleService.stopUsingNgIdle();
  }
}
