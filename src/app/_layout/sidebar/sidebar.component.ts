import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { environment } from "../../../environments/environment";
import { StorageService } from "../../secret/storage.service";


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private admin?: AdminService, private storageService?: StorageService,) { }
  breadScrum: string;
  loaderStatus: string;
  logoSrc: string = environment.imageUrl;
  roleId:number = this.storageService.secureStorage.getItem("roleId");
  ngOnInit() {
    this.admin.currentMessage.subscribe(res => {
      this.breadScrum = res;
    });
  }
}
