import { IdleService } from "./../services/idle.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { AdminDetail } from "../classes/admin-detail";
import { AdminService } from "../services/admin.service";
import { Router } from "@angular/router";
import { StorageService } from "../secret/storage.service";
import { ProgressBarMode } from "@angular/material/progress-bar";
import { ToastrService } from 'ngx-toastr';
import { environment } from "../../environments/environment";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  private adminDetail = new AdminDetail();
  mode: ProgressBarMode = "indeterminate";
  status: boolean = false;
  imagePath: String =  environment.imageUrl;

  constructor(
    private adminService: AdminService,
    private router: Router,
    private storageService: StorageService,
    private idleService: IdleService,
    private toastr?: ToastrService
  ) { }

  ngOnInit() {
    if (this.adminService.isLoggedIn()) {
      this.router.navigate(["/dashboard"]);
    } else {
      this.router.navigate(["/login"]);
    }
  }

  // create the form object.
  form = new FormGroup({
    uName: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required),
  });

  Login(LoginInformation) {
    this.adminDetail.userName = this.uName.value;
    this.adminDetail.password = this.Password.value;
    this.status = true;
    this.adminService.login(this.adminDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        let result = response.body.token;
        this.status = false;
        if (result.length) {
          this.storageService.secureStorage.setItem("token", result);
          this.storageService.secureStorage.setItem("userName",  response.body.userName);
          this.storageService.secureStorage.setItem("roleId",  response.body.roleProfile.roleId);
          this.router.navigate(["/dashboard"]);
        } else {
          alert(
            "please register before login Or Invalid combination of Email and password"
          );
        }
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          this.toastr.error("Invalid Credentials");
        } else {
          this.toastr.error("Server is not available");
        }
      }
    );
  }

  get uName() {
    return this.form.get("uName");
  }

  get Password() {
    return this.form.get("password");
  }
}
