import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { ErrorComponent } from './error/error.component';
import { AppComponent } from './app.component';
import { SiteLayoutComponent } from './_layout/site-layout/site-layout.component';
import { TestComponent } from './test/test.component';
import { UserSettingsComponent } from './_user/user-settings/user-settings.component'
import { RoleProfileComponent } from './_user/role-profile/role-profile.component'

import { AuthGuard } from './guards/auth-guard.service';


const routes: Routes = [
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      { path: '', component: DashboardComponent, pathMatch: 'full' },
      { path: 'configure App', component: ConfigurationComponent },
      { path: 'migration', component: MainComponent },
      { path: 'user Settings', component: UserSettingsComponent },
      { path: 'role Profile', component: RoleProfileComponent },
      { path: 'dashboard', component: DashboardComponent },

    ],
    canActivate: [AuthGuard]
  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: ErrorComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
