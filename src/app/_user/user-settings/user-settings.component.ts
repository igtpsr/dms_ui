import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from '../../services/admin.service';
import { Animations } from '../../animation';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from './user';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as moment from 'moment';




export interface UserList {
  email: string;
  userName: string;
  roleProfile: roleInterface;
}

interface roleInterface {
  roleId: number;
  roleName: string;
  modifyTs: string;
  modifyUser: string;
}

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css'],
  animations: [
    Animations.animeTrigger
  ]
})
export class UserSettingsComponent implements OnInit {

  displayedColumns: string[] = ['userName', 'email', 'roleProfile', 'symbol'];
  dataSource: MatTableDataSource<UserList>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  user = new User(null, '', '', '', null, null, '', '', '');


  userResonseList: any;
  roleResonseList:roleInterface[] = [];

  constructor(private Activrouter: ActivatedRoute, private adminService: AdminService, public dialog?: MatDialog, private toastr?: ToastrService) {
    this.Activrouter.url.subscribe(url => {
      this.adminService.changeMessage(url[0].path);
    });
  }

  ngOnInit(): void {
    this.adminService.startLoader();
    this.getAllUserProfile();
    this.getAllRole();
  }

  addNewEnv(flag: string, index?: number): void {
    let dialogTypeVar: string;
    flag == 'create' ? (this.user = this.blankUser(), dialogTypeVar = 'Add') : (this.user = this.userResonseList[index], dialogTypeVar = 'Update');
    const dialogRef = this.dialog.open(AddUser, {
      width: '380px',
      data: { userDetails: this.user, dialogType: dialogTypeVar, roleResonseList:this.roleResonseList },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      let resultValue: string = result;
      if (resultValue != '') {

        if (flag == 'create') {
          this.createUser();
        } else {
          this.updateUser();
        }
      }

    });
  }

  blankUser() {
    return new User(null, '', '', '', { roleId: 1, roleName: null, modifyTs: null, modifyUser: null }, null, '', '', '');
  }

  getAllUserProfile() {
    this.adminService.getAllUserProfile().subscribe(res => {
      this.adminService.endLoader();
      this.userResonseList = res;
      this.dataSource = new MatTableDataSource(this.userResonseList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  manageUser(index: any) {
    this.addNewEnv('update', index);
  }

  createUser() {
    this.user.passwordExpDate = moment(this.user.passwordExpDate).format("YYYY-MM-DDT00:00:00.000");
    this.adminService.startLoader();
    let filterValue:roleInterface[] = this.roleResonseList.filter((res)=>res.roleName ==this.user.roleProfile.roleName);
    this.user.roleProfile.roleId = filterValue[0].roleId;
    this.adminService.createUser(this.user).subscribe(res => {
      this.toastr.success('User Successfully Created');
      this.userResonseList = res;
      this.dataSource = new MatTableDataSource(this.userResonseList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.adminService.endLoader();
    })
  }

  updateUser() {
    this.user.passwordExpDate = moment(this.user.passwordExpDate).format("YYYY-MM-DDT00:00:00.000");
    this.adminService.startLoader();
    let filterValue:roleInterface[] = this.roleResonseList.filter((res)=>res.roleName ==this.user.roleProfile.roleName);
    this.user.roleProfile.roleId = filterValue[0].roleId;
    this.adminService.updateUser(this.user).subscribe(res => {
      this.toastr.success('User Successfully Updated');
      this.adminService.endLoader();
    })
  }

  getAllRole() {
    this.adminService.getAllRoleProfile().subscribe(res => {
      this.roleResonseList = res;
    })
  }


}

@Component({
  selector: 'add-user',
  templateUrl: 'add-user.html'
})
export class AddUser {
  hide:boolean = false;
  todayDate:Date = new Date();
  email = new FormControl('ssss', [Validators.required, Validators.email]);
  getErrorMessage() {
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
  // constructor(public dialogRef: MatDialogRef<AddNewEnvironment>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  constructor(public dialogRef: MatDialogRef<AddUser>, @Inject(MAT_DIALOG_DATA) public data) { }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
