
interface roleInterface {
    roleId:number;
    roleName:string;
    modifyTs:Date;
    modifyUser:string;
}
export class User {
    roleProfile:roleInterface;
    userId:number
    passwordExpDate:string
    constructor(userId:number, userName:string, password:string, email:string, roleProfile:roleInterface, passwordExpDate:string, modifyTs:string, modifyUser:string, token:string) {
        this.roleProfile = roleProfile;
        this.passwordExpDate = passwordExpDate;
    }
}