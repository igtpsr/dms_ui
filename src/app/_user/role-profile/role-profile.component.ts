import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Role} from './role'
import { Animations } from '../../animation';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


export interface RoleList {
  roleName: string;
  symbol: string;
}

const ELEMENT_DATA: RoleList[] = [
  { roleName: 'Babu Hussain', symbol: 'H' },
  { roleName: 'New User', symbol: 'He' },
  { roleName: 'Update User', symbol: 'Li' },
];


@Component({
  selector: 'app-role-profile',
  templateUrl: './role-profile.component.html',
  styleUrls: ['./role-profile.component.css'],
  animations: [
    Animations.animeTrigger
  ]
})


export class RoleProfileComponent implements OnInit {

  displayedColumns: string[] = ['roleName', 'symbol'];
  dataSource: MatTableDataSource<RoleList>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // dataSource = ELEMENT_DATA;

  roleResonseList: any;


  role = new Role('',null,'','');
  constructor(private Activrouter: ActivatedRoute, private adminService: AdminService, public dialog?: MatDialog , private toastr?: ToastrService) {
    this.Activrouter.url.subscribe(url => {
      this.adminService.changeMessage(url[0].path);
    });
  }

  ngOnInit(): void {
    this.adminService.startLoader();
    this.getAllRole();
    
  }

  addNewRole(flag: string, index?: number): void {
    let dialogTypeVar: string;
    // flag == 'create' ? (this.user = this.blankUser(), dialogTypeVar = 'Add') : (this.user = this.userResonseList[index], dialogTypeVar = 'Update');
    flag == 'create' ? (this.role =  this.blankRole(), dialogTypeVar = 'Add New'): (this.role = this.roleResonseList[index], dialogTypeVar = 'Update');
    const dialogRef = this.dialog.open(AddRole, {
      width: '380px',
      data: { roleDetails: this.role, dialogType: dialogTypeVar},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      let resultValue: string = result;
      console.log(result);
      if (resultValue != '') {
        if (flag == 'create') {
          this.createRoleProfile();
        } else {
          this.updateRoleProfile();
        }
      }
    });
  }

  blankRole() {
    return new Role('', null, '','');
  }
  getAllRole() {
    this.adminService.getAllRoleProfile().subscribe(res => {
      this.roleResonseList = res;
      this.dataSource = new MatTableDataSource(this.roleResonseList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.adminService.endLoader();
    })
  }

  manageRole(index: any) {
    this.addNewRole('update', index);
  }
  createRoleProfile() {
    this.adminService.startLoader();
    this.adminService.createRoleProfile(this.role).subscribe(res=>{
      this.toastr.success('Role Successfully Created');
      this.roleResonseList = res;
      this.dataSource = new MatTableDataSource(this.roleResonseList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.adminService.endLoader();
    })
  }

  updateRoleProfile() {
    this.adminService.startLoader();
    this.adminService.updateRoleProfile(this.role).subscribe(res=>{
      this.toastr.success('Role Successfully Updated');
      this.adminService.endLoader();
    })
  }



}

@Component({
  selector: 'add-role',
  templateUrl: 'add-role.html'
})
export class AddRole {
  // constructor(public dialogRef: MatDialogRef<AddNewEnvironment>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  constructor(public dialogRef: MatDialogRef<AddRole> , @Inject(MAT_DIALOG_DATA) public data) { }
  onNoClick(): void {
    this.dialogRef.close();
  }

}