import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AdminService } from '../services/admin.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _authService: AdminService, private _router: Router) {
  }

  // canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //   if (this._authService.isLoggedIn()) {
  //       return true;
  //   }

  //   // navigate to login page
  //   this._router.navigate(['/login']);
  //   // you can save redirect url so after authing we can move them back to the page they requested
  //   return false;
  // }

  canActivate():boolean {
    if (this._authService.isLoggedIn()) {
        return true;
    }

    // navigate to login page
    this._router.navigate(['/login']);
    // you can save redirect url so after authing we can move them back to the page they requested
    return false;
  }

}
