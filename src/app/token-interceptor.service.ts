import { Injectable, Injector } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";
import { StorageService } from "./secret/storage.service";
import { AdminService } from "./services/admin.service";
import { Router } from "@angular/router";
import { Observable, throwError, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: "root",
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(
    private router?: Router,
    private adminService?: AdminService,
    private storageService?: StorageService,
    private toastr?: ToastrService,
    public dialog?: MatDialog
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // We retrieve the token, if any
    const token = this.adminService.getToken();
    let newHeaders = req.headers;
    if (token) {
      newHeaders = newHeaders.append("Authorization", "Bearer " + token);
      const authReq = req.clone({ headers: newHeaders });
      if (req.headers.get("skip")) {
        return next
          .handle(req)
          .pipe(catchError((x) => this.handleAuthError(x)));
      } else {
        return next
          .handle(authReq)
          .pipe(catchError((x) => this.handleAuthError(x)));
      }
    } else {
      return next.handle(req);
    }
  }

  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    //handle your auth error or rethrow

    if (err.status === 401 || err.status === 403) {
      //navigate /delete cookies or whatever
      this.storageService.secureStorage.clear();
      this.router.navigateByUrl(`/login`);
      location.reload();
      // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
      return of(err.message); // or EMPTY may be appropriate here
    }
    if (err.status == 0) {
      this.storageService.secureStorage.clear();
      this.router.navigateByUrl(`/login`);
      location.reload();
      return of(err.message);
    } else {
      console.log('rrrrrr->>>',err);
      this.toastr.error(err.error.message,'',{ tapToDismiss: true , disableTimeOut:true});
      this.adminService.endLoader();
      this.dialog.closeAll();
    }
  }
}
